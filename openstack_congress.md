# OPENSTACK CONGRESS

## 1. Introduction

Congress is an OpenStack project to provide **policy as a service** across any collection of cloud services in order to **_offer governance and compliance for dynamic infrastructures_**.

### 1.1. Why Congress
IT services will always be governed and brought into compliance with business-level policies.
In the past, policy was enforced manually, e.g. by someone sending an email asking for an application to be added to the network, secured by specific firewall entries, connected to an agreed-on storage, and so on. 
In the cloud era, IT has become more agile: users expect immediate delivery of services, a level of responsiveness that is unattainable by the team responsible for governance. 
Hence, manual enforcement is no longer feasible.

Both enterprises and vendors have fielded engines for enforcing policy (semi)-automatically, creating a fragmented market where enterprises reinvent the wheel while maintaining their own code, and vendors fail to meet enterprise needs, either for technical reasons or because their solutions require vertical integration and lock-in.

The Congress policy service enables IT services to extend their OpenStack footprint by onboarding new applications while keeping the strong compliance and governance dictated by their own business policies.<br> 
All of that leveraging a community-driven implementation in which vendors can plug into a common interface.

### 1.2. What is Congress
Congress aims to provide an extensible open-source framework for **governance and regulatory compliance across any cloud services (e.g. application, network, compute and storage) within a dynamic infrastructure**.
It is a cloud service whose sole responsibility is **_policy enforcement_**.

Congress aims to include the following functionality:
- Allow cloud administrators and tenants to use a high-level, general purpose, declarative language to describe business logic.<br>
The policy language does not include a fixed collection of policy types or built-in enforcement mechanisms; 
    rather, a policy simply defines which states of the cloud are in compliance and which are not, where the state of the cloud is the collection of data provided by the cloud services available to Congress. <br>
Some examples:
    - Application A is only allowed to communicate with application B.
    - Virtual machine owned by tenant A should always have a public network connection if tenant A is part of the group B.
    - Virtual machine A should never be provisioned in a different geographic region than storage B.
- Offer a pluggable architecture that connects to any collection of cloud services
- Enforce policy
    - Proactively: preventing violations before they occur
    - Reactively: correcting violations after they occur
    - Interactively: give administrators insight into policy and its violations<br> 
        E.g: 
        - identifying violations 
        - explaining their causes 
        - computing potential remediations 
        - simulating a sequence of changes

### 1.3. Purposes
#### 1.3.1. Governance 
The first purpose of OpenStack Congress is governance which is to *use a high level declarative language to define the stat of the cloud infrastructure*. 
Puppet is a declarative language and so is OpenStack Heat Template.  
Declarative mean, only the desired end state is specified without giving detail or step by step instruction as to how to attain the desired end state.
<br>
The declarative language used by OpenStack Congress is Datalog which is basically SQL with syntax that is closer to traditional/procedural programming language.
Extracted from OpenStack Documentation the grammar of this declarative languages are:
```
<policy> ::= <rule>*
<rule> ::= <atom> COLONMINUS <literal> (COMMA <literal>)*
<literal> ::= <atom> 
<literal> ::= NOT <atom> 
<atom> ::= TABLENAME LPAREN <term> (COMMA <term>)* RPAREN 
<term> ::= INTEGER | FLOAT | STRING | VARIABLE
```

#### 1.3.2. Compliance
Another purpose of OpenStack Congress is policy enforcement.<br>  
There are 3 ways that the policy is enforced:
- *Proactively*: preventing violations before they occur
- *Reactively*: correcting violations after they occur
- *Interactively*: give administrators insight into policy and its violations, 
<br>E.g. 
    - identifying violations 
    - explaining their causes 
    - computing potential remediation 
    - simulating a sequence of changes.

## 2. Use cases
### 2.1. Use case 1: Public/private networks with group membership
There are a plethora of restrictions that cloud operators may want to place on how VMs are connected to networks.<br>  
For example, suppose we want to ensure that every network a VM is connected to is either a public network 
or the owner of the network and the other of the VM belong to the same ActiveDirectory/Keystone/etc. group.

**Data sources**:
- Neutron: the list of public networks and the owner of each network
- Nova: the list of networks connected to VMs and the owners of those VMs
- Keystone: which users are members of which groups

**Policy**
**error(vm):** 
- nova:instance(vm),
- nova:network(vm, network),
- not neutron:public(network),
- nova:owner(vm, vmowner),
- neutron:owner(network, netowner),
- not same_group(vmowner, netowner)
- same_group(x,y):
    - group(x,g),
	- group(y,g)
- group(x,g):
	- keystone:group(x, g)
- group(x,g):
	- ad:group(x,g)

**Policy Actions**
- Monitoring  (Could also illustrate proactive/reactive actions).

### 2.2. Use case 2: All VMs connected to the Internet must be secured

Every port on any VM connected to the internet must be assigned the security group “lockdown”.

**Data Sources**:
- Nova
- Neutron

**Policy**:
1. Define table that shows all the security group names for each port
port_security_group(port_id, sg_name):
    - neutron:ports(security_groups_id=sg_groups, id=port_id),
    - neutron:ports.security_groups(security_groups_id=sg_groups, security_group_id=sg_id),
    - neutron:security_groups(name=sg_name, id=sg_id)

2. Define table that shows which routers are connected to which networks
router_network(router_id, network_id):
    - neutron:routers(id=router_id),
    - neutron:ports(network_id=network_id, device_id=router_id)

3. Define table representing routers not on the internet
empty_external_gateway_router(router_id):
    - neutron:routers(external_gateway_info=\"None\", id=router_id)

4. Define table representing which device_ids are connected to the internet (via which ports)
connected_to_internet(device_id, port_id):
    - neutron:ports(network_id=network_id, id=port_id, device_id=device_id),
    - router_network(router_id, network_id),
    - not empty_external_gateway_router(router_id)

5. Define policy that says every port on every VM connected to the internet must be assigned the security group named "lockdown"
error(vm):
    - nova:servers(id=vm),
    - connected_to_internet(vm, port),
    - not port_security_group(port, \"lockdown\")

**Policy Actions Needed**:
- Monitoring

### 2.3. Use case 3: Identify underutilized servers
Find those servers whose CPU utilization over the last 2 months was on average less than 5%.<br>
Such servers are being underutilized and could potentially be reclaimed.  Send an email to the owners of those VMs.

**Data Sources**
- Nova: for info about compute nodes and their owners
- Ceilometer: for CPU utilization statistics
- Keystone: for grabbing a user’s email

**Policy**
- error(vm, email):
	- nova:server_owner(vm, owner),
	- two_months_before_today(start, end),
   	- ceilometer:statistics(vm, start, end, “cpu-util”, cpu),
	- cpu < 5,
	- keystone:email(owner, email)
- two_months_before_today(start, end):
	- date:today(end),
	- date:minus(end, “2 months”, start)

**Policy Actions Needed**
<br>What type of action does Congress need to take for this use case?  
Examples: 
- Monitoring
- Reactive: Computing/executing actions that bring the cloud back into compliance

### 2.4. Use case 4: Automatic Evacuation on Host Failure
This use case describes the scenario when a host is down and the vms running inside have been tagged to have full availability.<br>
The cloud operator will be able to define at project or vm level if this has to be evacuated in case of a host failure. 
Once the host fails, this event is notified and all the vms which are tagged or belong to a tenant which is tagged, will be automatically evacuated to a different host, if any.<br>
The time that the machines were out of service until they were evacuated will be considered as part of the QoS metrics and SLA. 

**Data sources**
- Nova: which VM is located on which server, move VM from one server to another
- RDBMS: which VM is high-availability
- Ceilometer?: which hosts are down

**Policy**
<br>error(vm): 
- nova:virtual_machine(vm),
- nova:server(vm, server),
- rdbms:high_available(vm),
- ceilometer:server_down(server)

**Policy Actions**
- Monitoring
- Reactive

**proposed features (pre-spec/blueprints details):**
<br>1) disaster recovery for stateful virtual machines policy
- problem description: host is down and the vms running inside have been tagged to have full availability
- proposed change/how to solve: By not using local disk to run the VMs, the VMs can be restarted on another compute node. Once heartbeat failure is detected, the dead hypervisor node would be taken out of the cluster, and a monitor service would schedule the VMs to be restarted. 
- alternatives: aggressively attempt to restart the dead hypervisor node through IPMI or PDU hard restart.
- data model impact: TBD
- API impact: TBD
- security impact: TBD
- implementation plan: TBD
- dependencies: TBD

### 2.5. Use case 5: Compromised VM
IDS service notices malicious traffic originating from an insider VM trying to send packets to hosts inside and outside of the tenant perimeter.<br>  
As this is detected, some reactive response would need to be taken, such as isolating the offending VM from the rest of the network.<br>
This policy would facilitate one of the reactive responses to be invoked when a compromise is reported by an IDS service.

**Data Sources**
- IDS (intrusion detection service VM): IP address of the offending VM
- RepDBMS (reputation DBMS): database of known bad external IP addresses (may be replaced by API-based real time checks from many public blacklists)

**Policy**
error(vm):
- nova:virtual_machine(vm),
- ids:ip_packet(src_ip, dst_ip),
- neutron:port(vm, src_ip),	//finds out the port that has the VM’s IP
- rep_dbms:ip_blacklist(dst_ip).

**Policy Actions**
- Monitoring: report/log the incident including the VM’s IP address, external IP, etc.
- Reactive: Invoke the neutron API to add a new rule to the port’s security group that blocks all traffic to/from the VM’s IP address (this is one of multiple ways of isolating the compromised VM)

### 2.6. Use case 6: Vulnerable software
Vulnerability scanner reports a VM running an unpatched OS or application software, for instance, when a new vulnerability is reported and a patch is made available by the software vendor.<br>  
As this is reported, some reactive measures would need to be taken, such as redirecting the traffic for this VM to another.<br>
This policy would facilitate a reactive response to be taken when such an incident is reported.

**Data Sources**
- VulScanner: <IP, port> that runs a vulnerable service
- NVD (national vulnerability database)
- ???Info about pre-conditions and post-conditions

More specific information needed. For example pre-condition, post-condition of the exploit. 

**Policy**
error(vm):
- nova:virtual_machine(vm),
- vulscanner:vulnerable(vul_ip, vul_port),
- neutron:port(vm, vul_ip).

**Policy Actions**
- Monitoring: log/report when a new vulnerable service is reported
- Reactive: take one of many corrective actions based on configuration parameters.
    - Option 1: deny all incoming traffic to this service ip/port
    - Option 2: install a new patched service in a new VM and redirect the traffic

### 2.7. Use case 7: Honeypot redirection for inbound traffic
IDS service sees some suspicious inbound traffic and unsure of its legitimacy but needs a reactive measure such as redirecting the traffic to a honeypot created and deployed on-the-fly to understand it better.<br>  
This policy needs to facilitate such mechanisms.

**Data Sources**
<br>IDS: <src_ip:src_port, dst_ip:dst_port> for the suspicious traffic

**Policy**

**Policy Actions**
- Monitoring
- Reactive: Use sub-policy implemented by Neutron and HoneyPot service

### 2.8. Use case 8: Virtual Machines Placement
There are many examples where one would want Prod VMs to be on only Prod Hypervisor and no dev/test VMs (PCI for example), 
or a Virtual Machine that needs lot of throughput should go on a server that has 10G NIC and is not too utilized

**Data Sources**
- Nova
- Neutron

**Policy**
<br>error(vm): 
- nova:instance(vm), 
- nova:stage(vm,stagevm),
- nova:compute(server), 
- nova:stage(server,stageserver),
- not same_stage_group(stagevm, stageserver)
<br>same_stage_group(x,y):
- stage_group(x, g),
- stage_group(y, g)
<br>stage_group(“dev”, “devtest”)
<br>stage_group(“test”, “devtest”)
<br>stage_group(“prod”, “prod”)

**Policy Actions Needed**
What type of action does Congress need to take for this use case?<br>  
Examples: 
- Proactive: Responding to queries from other components about whether a given API call will cause new violations.
- Reactive: Computing/executing actions that bring the cloud back into compliance

### 2.9. Use case 9: Placement and Scheduling for Cloud & NFV Systems based on Policy Constraints (PSCN)
The goal of PSCN is optimized placement and scheduling of Cloud/NFV systems based on resource constraints and service request requirements.<br> 
The initial key goals are as follows:
- IaaS scheduling across DCs of service provider #1 for supporting asynchronous replication model for state data as specified in the service request requirements by service provide #2 [NFV-USE-CASE]. The primary goal is to optimize resource utilization of network and energy in the infrastructure of service providers #1and #2. This applies to scheduling within a provider too.
- NFVIaaS placement across distributed NFV DCs of service provider #1 with various resource constraints such as compute, network and service request requirements as specified by service provider #2 [NFV-USE-CASE]. The primary goal is to optimize resource utilization of compute and network in the NFV infrastructure of service provider #1. This applies to placement within a provider too.

**Data Sources**
- Nova (Compute): VM – Tenant, Virtual Network Function (VNF), Server, NFV Data Center, Service Provider (needed for inter-provider use cases)
- Neutron (Network): Inter-DC Network - Tenant, Virtual Network Function (VNF), Service Provider (needed for inter-provider use cases), static network bandwidth, dynamic network bandwidth utilization
- Nova (Energy): Server power consumption in various idle states, Dynamic power consumption in servers (leverage IPMI or other standards)

**Policy**
- Hourly/daily/weekly time windows for scheduling certain category of application workloads (a good example would be asynchronous replication across DCs)
- Restrict VM placement only to certain DCs
- Maximum energy consumption per DC
- Dedicate capacity for example VMs in a DC, inter-DC network bandwidth etc. for handling certain types of workloads

**Policy Actions**
- Notify applications of precise time windows (order of minutes) for scheduling workloads

### 2.10. Use case 10: Security Group Management
Security group management policies check against existing security group properties.<br> 
In our implementation at Symantec, we alert security groups with no incoming traffic control.<br> 
We trace such violation at both tenant and instance level. We also have policies to further highlight those violated VMs with internet access.

**Data Sources:**
- Nova
- Neutron

**Policies:**
<br>// Name: tenants with no ingress control for certain ports
- ingress_free_tenant(tenant_id, security_group_id, protocol, ethertype, port_range_min, port_range_max):- 
- neutronv2:security_group_rules(security_group_id, id, tenant_id, remote_group_id, "ingress", ethertype, protocol, port_range_min, port_range_max, "0.0.0.0/0")

<br>// Name:  instances with no ingress control for certain ports
- ingress_free_vm(vm_id, tenant_id, security_group_id):- neutronv2:security_group_port_bindings (port_id, security_group_id), 
- neutronv2:ports(port_id, tenant_id, name, network_id, mac_address, admin_state_up, status, vm_id, port_type), 
- ingress_free_tenant(tenant_id, security_group_id, protocol, ethertype, port_range_min, port_range_max)

<br>// Name: instances with internet access
<br>connected_to_internet(port_id, vm_id):
- neutronv2:external_gateway_infos(router_id=router_id, network_id=network_id_gw), neutronv2:ports(network_id = all_network, device_id = router_id), 
- neutronv2:ports(network_id = all_network, id=port_id, device_id=vm_id), nova:servers(id=vm_id)

<br>// Name: violated instances with internet access
<br>external_ac_ingress_free_vm(vm_id, port_id): 
- connected_to_internet(port_id, vm_id), ingress_free_vm(vm_id, tenant_id, security_group_id)

**References:**
- [NFV-USE-CASE] “ETSI GS NFV 001Network Functions Virtualization (NFV); Use Cases,” http://www.etsi.org/deliver/etsi_gs/NFV/001_099/001/01.01.01_60/gs_NFV001v010101p.pdf
- [NFV-ARCH] “NFV Architectural Framework,” http://www.etsi.org/deliver/etsi_gs/NFV/001_099/002/01.01.01_60/gs_NFV002v010101p.pdf
- [NFV-REQ] “NFV Virtualization Requirements,” http://www.etsi.org/deliver/etsi_gs/NFV/001_099/004/01.01.01_60/gs_NFV004v010101p.pdf

### 2.11. Use case 11: Close All Ports Open for Ingress
People don't understand security, so they create VMs and leave all the ports open; 
find VMs on internal network where all the ports are open and then change them.

**Data Sources**
- Nova
- Neutron

**Policy**
- A list of datalog rules that represent the policy

**Policy Actions Needed**
- Monitoring
- Proactive:
- Reactive:
- Assistive:
- Sub-policy:  

### 2.12. Use case 12: Prevent a VM From Bridging 2 Networks With Different Security Levels
Prevent a VM from bridging 2 networks of different security levels – e.g. Internet and Conexus.<br> 
Any time a floating IP is added to a VM or a vNIC, check the other networks that vm has access to and if a conflict is found, disable one or both network connections or disable the vm.

**Data Sources**
- Nova
- Neutron
- Conexus
- Data Center subnets
- Corporate LAN

**Policy**
- A list of datalog rules that represent the policy

**Policy Actions Needed**
- Monitoring
- Proactive:
- Reactive:
- Assistive:
- Sub-policy:

**Steps To implement the use case described:**
1. Identify the subnets (by CIDR) that are of various purposes / security levels / sensitivity
- Internet/DMZ
- Conexus
- Mobility or other service data center subnets (admin, monitoring, service)
- Corporate LAN
2. Identify any conditions under which a workload (in a VM or container) should not be connected to a set of those subnets, e.g.
- When also connected to the Internet directly, e.g. in a DMZ subnet
- When started on behalf of a specific or class of tenant, e.g.
    - any non-AT&T tenant (meaning any workload other than those supporting an AT&T-internal service)
    - a specific tenant (e.g. an MVNO)

There might be some gotchas that go beyond Congress’ ability to govern this, e.g. routability thru connected devices and the presence (or not) of firewalls in the path. But security is a layered thing, e.g. involving
- local security on the VMs/containers starts with iptables (based upon OpenStack Security Groups)
- control-plane security i.e. things that the control plane can learn about and react to – this is where Congress can directly add value, including spanning multiple control plane elements / zones
- perimeter security and firewalls in general
- background audits e.g. intrusion and vulnerability detection (a vulnerability can be found by detecting a routable connection that shouldn’t be)

https://docs.google.com/document/d/1ExDmT06vDZjzOPePYBqojMRfXodvsk0R8nRkX-zrkSw/edit#heading=h.fbota2qx0jad

### 2.13. Use case 13: Neutron Group-Based Policy (GBP)
Neutron Group-Based Policy (GBP), which is similar to the policy effort in OpenDaylight, utilizes policy to manage networking. 
A policy describes how the network packets in the data center are supposed to behave. 
Each policy (“contract” in GBP terminology) describes which actions (such as allow, drop, reroute, or apply QoS) should be applied to which network packets based on packet header properties like port and protocol. 
Entities on the network (called “endpoints”) are grouped and each group is assigned one or more policies. 
Groups are maintained outside the policy language by people or automated systems.
<br>
In GBP, policies can come from any number of people or agents. 
Conflicts can arise within a single policy or across several policies and are eliminated by a mechanism built into GBP (which is out of scope for this blog post).
<br>
The goal of GBP is to enforce policy directly. 
(Both monitoring and auditing are challenging in the networking domain because there are so many packets moving so quickly throughout the data center.) 
To do enforcement, GBP compiles policies down into existing Neutron primitives and creates logical networks, switches, and routers. 
When new policy statements are inserted, GBP does an incremental compilation: changing the Neutron primitives in such a way as to implement the new policy while minimally disrupting existing primitives.

### 2.14. Use case 14: Swift Storage Policy
Swift is OpenStack’s object storage service. 
As of version 2.0, released July 2014, Swift supports storage policies. 
Each storage policy is attached to a virtual storage system, which is where Swift stores objects. 
**Each policy assigns values to a number of built-in features of a storage system**. 
At the time of writing, each policy dictates how many partitions the storage system has, how many replicas of each object it should maintain, and the minimum amount of time before a partition can be moved to a different physical location since the last time it was moved.
<br>
A user can create any number of virtual storage systems—and so can write any number of policies—but there are no conflicts between policies. 
If we put an object into a container with 2 replicas and the same object into another container with 3 replicas, 
it just means we are storing that object in two different virtual storage systems, which all told means we have 5 replicas.
<br>
Policy is enforced directly by Swift. 
Every time an object is written, Swift ensures the right number of replicas are created. 
Swift ensures not to move a partition before policy allows that partition to be moved.

### 2.15. Use case 15: Smart Scheduler/SolverScheduler
The Smart Scheduler/SolverScheduler effort aims to provides an interface for using different constraint solvers to solve optimization problems for other projects, Nova in particular. 
One specific use case is for Network Functions Virtualization. 
For example, Nova might ask where to place a new virtual machine to minimize the average number of VMs on each server. 
This effort utilizes domain-independent solvers (such as linear programming/arithmetic solvers) but applies them to solve domain-specific problems. 
The intention is to focus on enforcement.

### 2.16. Use case 16: Heat Convergence engine
The Heat Convergence engine represents a shift toward a model for Heat where applications are deployed and managed by comparing the current state of the application to the desired state of the application and taking action to reduce the differences. 
Each desired state amounts to a policy describing a single application. 
Those policies do not interact, logically, and can draw upon any service in the data center. 
Heat policies are concerned mainly with corrective enforcement, though monitoring is also useful (“how far along is my application’s deployment?”).

## 3. Functional Description
### 3.1. Supported Drivers for OpenStack
- OpenStack Aodh
- OpenStack Cinder
- OpenStack Glance (v2)
- OpenStack Heat
- OpenStack Ironic
- OpenStack Keystone (v2 & v3)
- OpenStack Mistral
- OpenStack Monasca
- OpenStack Monasca Webhook (unstable schema: may change in future release)
- OpenStack Murano
- OpenStack Neutron (v2)
- OpenStack Neutron QoS
- OpenStack Nova
- OpenStack Swift
- OpenStack Vitrage (unstable schema: may change in future release)
- NFV Doctor
- Cloud Foundry (unofficial)
- Plexxi (unofficial)
- vCenter (unofficial)

### 3.2. Concept
**Existing Approach: Multiple Touch Points**
![approach before congress](./image/approach_before_congress.png)

**Congress Policy Framework**
![congress policy framework](./image/congress_policy_framework.png)

**Any Cloud Service**
![any cloud service](./image/any_cloud_service.png)

**Any Policy**
![any policy](./image/any_policy.png)

**Monitoring and Enforcement**
![monitoring and enforcement](./image/monitoring_and_enforcement.png)

#### 3.2.1. What are the policy sources a policy system must accommodate?
Let’s start by digging deeper into an idea we touched on in the first post when describing the challenge of policy compliance: the sources of policy. 
While we sometimes talk about there being a single policy for a data center, the reality is that there are really many different policies that govern a data center. 
Each of these policies may have a different source or origin. Here are some examples of different policy sources:
- Application developers may write a separate policy for each application describing what that application expects from the infrastructure (such as high availability, elasticity/auto-scaling, connectivity, or a specific deployment location).
- The cloud operator may have a policy that describes how applications relate to one another. 
This policy might specify that applications from different business units must be deployed on different production networks, for example.
- Security and compliance teams might have policies that dictate specific firewall rules for web servers, encryption for PCI-compliant workloads, or OS patching guidelines.
- Different policies may be focused on different functionality within the data center. 
There might be a deployment policy, a billing policy, a security policy, a backup policy, and a decommissioning policy.
- Policies may be written at different levels of abstraction 
<br>E.g. 
    - applications versus virtual machines (VMs), 
    - networks versus routers, 
    - storage versus disks.
- Different policies might exist for different policy operations (monitoring, enforcing, and auditing are three examples that we will discuss later in this post).
<br>
The idea of multiple sources of policy naturally leads us to the presence of multiple policies. 
This is an interesting idea, because these multiple policies can interact with each other in many different ways. 
A policy describing where an application is to be deployed might also implicitly describe where VMs are to be deployed. 
A cloud operator’s policy might require an application to be deployed on network A or B, and an application policy requiring high availability might mean it must be deployed on network B or C; taken together, this means the application can only be deployed on network B. 
An auditing policy that requires knowing provenance for data when applied to an application that supports a high transaction rate might require solid state storage to meet performance requirements.
<br>
Taking this a step further, it may be unclear how these policies should interact. 
If the backup policy says to have 3 copies of data, and an auditing policy requires keeping track of where the data originated, do we need 3 copies of that provenance information? Conflicts are another example. 
If the application’s policy implies networks A or B, and the cloud operator’s policy implies networks C or D, then there is no way to deploy that application so that both policies are satisfied simultaneously.
<br>
There are a couple key takeaways from this discussion. 
- First, a policy system must deal with multiple policy sources. 
- Second, a policy system must deal with the presence of multiple policies, and how those policies can or should interact with one another.

#### 3.2.2. How do those sources express the desired policy to the system?
Any discussion of policy systems has to deal with the subject of policy languages. 
An intuitive, easy-to-use syntax is critically important for eventual adoption, but here we focus on more semantic issues: how domain-specific is the language? How expressive is the language? What general-purpose policy features belong to the language?

A language is a domain specific language (DSL) if it includes primitives useful for policy in one domain but not another. 
For example, a policy language for networking might include primitives for the source and destination IP addresses of a network packet. 
A DSL for compute might include primitives for the amount of memory or disk space on a server. 
An application-oriented DSL might include elasticity primitives so that how different parts of the application grow and shrink can be the subject of policy.
<br>
Different parts of a policy language can be domain-specific:
- Namespace: The objects over which we declare policy can be domain-specific. 
For example, a networking DSL might define policy about packets, ports, switches, and routers.
- Condition: Policy languages typically have if-then constructs, and the “if” part of those constructs can include domain-specific tests, such as the source and destination IP addresses on a network packet.
- Consequent: The “then” component of if-then constructs can also be domain-specific. 
For networking, this might include allowing/dropping a packet, sending a packet through a firewall and then a load balancer, or ensuring quality of service (QoS).
- Independent of domain-specific constructs, a language has a fundamental limitation on its expressiveness (its “raw expressiveness”). Language A is more expressive than language B if every policy for B can be translated into a policy for A but not vice versa. For example, if language A supports the logical connectives AND/OR/NOT, and language B is the same except it only supports AND/OR, then A is more expressive than B. 
However, it can be the case that language A supports AND/OR/NOT, language B supports AND/NOT, and yet the two languages are equally expressive (because OR can be simulated with AND/NOT).
<br>
It may seem that more expressiveness is necessarily better because a more expressive language makes it easier for users to write policies. 
Unfortunately, the more expressive a language, the harder it is to implement. 
By “harder to implement” we don’t mean it’s harder to get a 30% speed improvement through careful engineering; rather, we mean that it is provably impossible to make the implementation of sufficiently expressive languages run in less than exponential time. In short, every policy language chooses how to balance the policy writers’ need for expressiveness and the policy system’s need for implementability.
<br>
On top of domain-specificity and raw expressiveness, different policy languages support different features. 
For example, can we say that some policy statements are “hard” (can never be violated) while other statements are “soft” (can be violated if the only way to not violate is to violate a hard constraint). 
More generally, can we assign priorities to policy statements? Is there native support for exceptions to policy rules (maybe a cloud owner wants to manually make an exception for a violation so that auditing reflects why that violation was less severe than it may have seemed). 
Does the language have policy modules and enable people to describe how to combine those modules to produce a new policy? 
While such features might not impact domain-specificity or raw expressiveness, they have a large impact on how easy the policy language—and therefore the system using that language—is to use.

The key takeaway here is that the policy language has a significant impact on the policy system, so the choice of policy language is a critical one.

#### 3.2.3. How does the policy system interact with data center services?
A policy system by itself is useless; to have value, the policy system must interact and integrate with other data center or cloud services. 
By “data center service” or “cloud service” we mean basically anything that has an API, e.g. OpenStack components like Neutron, routers, servers, processes, files, databases, antivirus, intrusion detection, inventory management. 
Read-only API calls enable a policy system to see what is happening; read/write API calls enable a policy system to control what is happening.
<br>
Since a policy system’s ability to do something useful with a policy (like prevent violations, correct violations, or monitor for violations) is directly related to what the service can see and do in the data center, it’s crucial to understand how well a policy system works with the services in a data center. 
If two policy systems are equivalent except that one works with a broader range of data center services than the other, the one with a broader selection of data center services has the ability to see and do more; thus, it’s better able to see and do things to help the data center obey policy.
<br>
One type of data center service is especially noteworthy: the policy-aware service. Such services understand policy natively. 
They may have an API that includes “insert policy statement” and “delete policy statement”. 
Such services are especially useful in that they can potentially help the data center obey certain kinds of sub-policies. 
Distributing the work makes a policy system more robust, more reliable, and better performing.

The key point to remember here is that a policy system’s “power” (knowledge of and control over what’s happening in a data center or cloud environment) is driven by the nature of its interaction with the services running in that data center.

#### 3.2.4. What can the policy system do once it has the policy?
Having looked at three key aspects of a policy system—supporting multiple sources of policies and multiple policies, 
using a policy language that balances expressiveness with implementability, 
and providing the appropriate depth and breadth of integration with necessary data center services—we now come to a discussion of what the policy system does (or can do) once it knows what policy (or group of policies) is pertinent to the data center. 
It’s compelling to think about the utility of policy in terms of the future, the present, and the past. 
We want the data center to obey the policy at all points in time, and there are different mechanisms for doing that.
- Auditing: We cannot change the past but we can record how the data center behaved, what the policy was, and therefore what the violations were.
- Monitoring: The present is equally impossible to change (by the time we act, that moment in time will have become the past), but we can identify violations, help people understand them, and gather information about how to reduce violations in the future.
- Enforcement: We can change the future behavior of the data center by taking action. Enforcement can attempt to prevent violations before they occur (“proactive enforcement”) or correct violations after the fact (“reactive enforcement”). This is the most challenging of the three because it requires choosing and executing actions that affect the natural state of the data center.
<br>
The potential for any policy system to carry out these three functions depends crucially on two things: the policy itself (a function of how well the system supports multiple policies as well as the system’s choice of policy language) and the controls the policy system has over the data center (driven by the policy system’s interaction with and integration into the surrounding data center services). 
The combination of these two things impose hard limits on how well any policy system is able to audit, monitor, and enforce policy.
<br>
While we would rather prevent violations than correct them, it’s sometimes impossible to do so. 
For example, we cannot prevent violations in a policy that requires server operating system (OS) instances to always have the latest patches. 
Why? As soon as Microsoft, Apple, or Red Hat releases a new security patch, the policy is immediately violated. 
The point of this kind of policy is that the policy system recognizes the violation and applies the patch to the vulnerable systems (to correct the violation). 
The key takeaway from this example is that preventing violations requires the policy system is on the critical path for any action that might violate policy. 
Violations can only be prevented if such enforcement points are available.
<br>
Similarly, it’s not always possible to correct violations. 
Consider a policy that says the load on a particular web server should never exceed 10,000 requests per second. 
If the requests to that server become high enough (even with load balancing), there may be no way reduce the load once it reaches 10,001 requests per second. 
The data center cannot control what web sites people in the real world access through their browsers. 
In this case, the key takeaway is that correcting violations requires there be actions available to the policy system to counteract the causes of those violations.
<br>
Even policy monitoring has limitations. 
A policy dictating application deployment to particular data centers based on the users of that application assumes readily available information about where applications are deployed and the users of those applications. 
A web application that does not expose information about its users ensures even monitoring this policy is impossible. 
The key takeaway here is that monitoring a policy requires that the appropriate information is available to the policy system. 
Further, if we cannot monitor a policy, we also cannot audit that policy.
<br>
In short, **every policy system has limitations**. These limitations might be on what the policy system knows about the data center, or these limitations might be on what control it has over the data center. 
These limitations influence whether any given policy can be audited, monitored, and enforced. 
Moreover, these limitations can change as the data center changes. As new services (hardware or software) are installed or existing services are upgraded in the data center, new functionality becomes available, and a policy system may have additional power (fewer limitations). When old services are removed, the policy system may have less power (more limitations).
<br>
These limitations give us ceilings on how successful any policy system might be in terms of auditing, monitoring, and enforcing policy. 
It is therefore useful to compare policy system designs in terms of how close to those ceilings they can get. 
Of course, the true test is in terms of actual implementation, not design, and a comparison of systems in terms of what policies they can audit, monitor, and enforce at scale is incredibly valuable. 
However, we must be careful not to condemn systems for not solving unsolvable problems.

## 4. Components
### 4.1. Cloud Services, Drivers, and State
_A service is anything that manages cloud state_. 
For example, OpenStack components like Nova, Neutron, Cinder, Swift, Heat, and Keystone are all services. 
Software like ActiveDirectory, inventory management systems, anti-virus scanners, intrusion detection systems, and relational databases are also services.
<br>
_Congress uses a driver to connect each service to the policy engine_. 
A driver fetches cloud state from its respective cloud service, and then feeds that state to the policy engine in the form of tables. 
A table is a collection of rows; each row is a collection of columns; each row-column entry stores simple data like numbers or strings.
<br>
For example, the Nova driver periodically makes API calls to Nova to fetch the list of virtual machines in the cloud, and the properties associated with each VM. 
The Nova driver then populates a table in the policy engine with the Nova state. For example, the Nova driver populates a table like this:
![congress nova driver](./image/congress_nova_driver.png)
<br>The state for each service will be unique to that service. 
- For Neutron, the existing logical networks, subnets, and ports make up that state. 
- For Nova, the existing VMs along with their disk and memory space make up that state. 
- For an anti-virus scanner, the results of all its most recent scans are the state.

### 4.2. Policy
A Congress policy defines **all those states of the cloud that are permitted**: 
all those combinations of service tables that are possible when the cloud is behaving as intended. 
Since listing the permitted states explicitly is an insurmountable task, 
policy authors describe the permitted states implicitly by writing a collection of if-then statements that are always true when the cloud is behaving as intended.
<br>
More precisely, Congress uses Datalog as its policy language. 
Datalog is a declarative language and is similar in many ways to SQL, Prolog, and first-order logic. 
Datalog has been the subject of research and development for the past 50 years, which means there is a wealth of tools, algorithms, and deployment experience surrounding it.
### 4.3. Capabilities
Once Congress is given a policy, it has three capabilities:
- monitoring the cloud for policy violations
- preventing violations before they occur
- correcting violations after they occur

In the future, Congress will also record the history of policy and its violations for the purpose of audit.
### 4.4. Congress Server and API
Congress runs as a standalone server process and presents a RESTful API for clients; drivers run as part of the server.
<br>The API allows clients to perform the following operations:
- insert and delete policy statements
- check for policy violations
- ask hypothetical questions: if the cloud were to undergo these changes, would that cause any policy violations?
- execute actions

## 5. Refs
- https://www.openstack.org/assets/presentation-media/Congress-OpenStack-Atlanta-2014.pdf