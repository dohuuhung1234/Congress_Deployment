#!/bin/bash
source config_information.sh

apt update
apt install crudini -y
apt install python-pip default-jre git gcc python-dev python-antlr3 libxml2 libxslt1-dev libzip-dev build-essential libssl-dev libffi-dev python-setuptools -y
git clone -b stable/$openstack_release https://github.com/openstack/congress.git
git clone -b stable/$openstack_release https://github.com/openstack/python-congressclient.git

cd congress
pip install -r requirements.txt
python setup.py install
sudo mkdir -p /etc/congress
sudo chmod 755 /etc/congress
sudo mkdir /var/log/congress
sudo chmod 755 /var/log/congress

cp etc/api-paste.ini /etc/congress
cp etc/policy.json /etc/congress
cp ../congress.conf /etc/congress
cp -R library /etc/congress/

crudini --set /etc/congress/congress.conf DEFAULT transport_url "rabbit://openstack:"$rabbitmq_password"@"$host_ip
crudini --set /etc/congress/congress.conf service_credentials auth_url "http://"$host_ip":35357"
crudini --set /etc/congress/congress.conf service_credentials password $congress_user_password
crudini --set /etc/congress/congress.conf database connection "mysql+pymysql://congress:"$database_congress_password"@"$host_ip"/congress"
crudini --set /etc/congress/congress.conf keystone_authtoken memcached_servers $host_ip":11211"
crudini --set /etc/congress/congress.conf keystone_authtoken auth_uri "http://"$host_ip":5000"
crudini --set /etc/congress/congress.conf keystone_authtoken password $congress_user_password
crudini --set /etc/congress/congress.conf keystone_authtoken auth_url "http://"$host_ip":35357"

sudo mysql -e "CREATE DATABASE congress;"
sudo mysql -e "GRANT ALL PRIVILEGES ON congress.* TO 'congress'@'localhost' IDENTIFIED BY '$database_congress_password';"
sudo mysql -e "GRANT ALL PRIVILEGES ON congress.* TO 'congress'@'%' IDENTIFIED BY '$database_congress_password';"

export http_proxy=""
export https_proxy=""
source $openstack_variable_file
sudo congress-db-manage --config-file /etc/congress/congress.conf upgrade head

ADMIN_ROLE=$(openstack role list | awk "/ admin / { print \$2 }")
SERVICE_TENANT=$(openstack project list | awk "/ service / { print \$2 }")
CONGRESS_USER=$(openstack user create --password $congress_user_password --project service congress | awk "/ id / {print \$4 }")
openstack role add $ADMIN_ROLE --user $CONGRESS_USER --project  $SERVICE_TENANT
CONGRESS_SERVICE=$(openstack service create policy --name congress --description "Congress Service" | awk "/ id / { print \$4 }")

openstack endpoint create congress --region RegionOne  public http://$host_ip:1789
openstack endpoint create congress --region RegionOne  admin http://$host_ip:1789
openstack endpoint create congress --region RegionOne  internal http://$host_ip:1789

cd ../python-congressclient
source ../config_information.sh
pip install -r requirements.txt
python setup.py install
pip install sphinx
pip install oslosphinx

export http_proxy=""
export https_proxy=""
source $openstack_variable_file
congress-server --config-file /etc/congress/congress.conf --log-dir /var/log/congress/ --node-id congressNode &
openstack congress datasource create glancev2 "glancev2" --config username=congress --config password=$congress_user_password --config tenant_name=service --config auth_url=http://$host_ip:5000/v3
openstack congress datasource create neutronv2 "neutronv2" --config username=congress --config password=$congress_user_password --config tenant_name=service --config auth_url=http://$host_ip:5000/v3
openstack congress datasource create nova "nova" --config username=congress --config password=$congress_user_password --config tenant_name=service --config auth_url=http://$host_ip:5000/v3
